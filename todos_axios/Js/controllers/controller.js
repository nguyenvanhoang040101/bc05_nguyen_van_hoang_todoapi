function renderTodoList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    var contentTr = `
      
      <tr>
      <td>${item.id}</td>
      <td>${item.name}</td>
      <td>${item.desc}</td>
      <td>
      <input type="checkbox" ${item.isComplete ? "checked" : ""} />
      </td>
      <td>
      <button onclick="removeTodo(${
        item.id
      })" class="btn btn-danger">Delete</button>
      <button onclick="editTodo(${
        item.id
      })" class="btn btn-success">Edit</button>



      </td>
      </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}

////
function layThongTinTuForm() {
  var name = document.getElementById("name").value;
  var desc = document.getElementById("desc").value;
  return {
    name: name,
    desc: desc,
  };
}
