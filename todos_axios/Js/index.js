const BASE_URL = "https://635f4b18ca0fe3c21a991d6d.mockapi.io";

var isEdited = null;
function fetAllTodo() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();

      renderTodoList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();

      console.log("err: ", err);
    });
}
fetAllTodo();
// remove todo service
function removeTodo(idTodo) {
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "Delete",
  })
    .then(function (res) {
      fetAllTodo();

      console.log(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}
//
function addTodo() {
  var data = layThongTinTuForm();

  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  turnOnLoading();

  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      turnOffLoading();

      fetAllTodo();

      console.log(res);
    })
    .catch(function (err) {
      turnOffLoading();

      console.log(err);
    });
}

//
function editTodo(idTodo) {
  turnOnLoading();

  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();

      //show thông tin lên form
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;
      isEdited = res.data.id;
    })
    .catch(function (err) {
      turnOffLoading();

      console.log(err);
    });
}
function updateTodo() {
  var data = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/todos/${isEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log(res);
      fetAllTodo();
    })
    .catch(function (err) {
      console.log(err);
    });
}
